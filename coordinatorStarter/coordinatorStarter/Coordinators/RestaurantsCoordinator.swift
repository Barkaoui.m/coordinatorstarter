//
//  RestaurantsCoordinator.swift
//  ShuffleEat
//
//  Created by Malek BARKAOUI on 13/08/2020.
//  Copyright © 2020 Malek BARKAOUI. All rights reserved.
//

import Foundation
import UIKit

class RestaurantsCoordinator: Coordinator {
    
    var parentCoordinator: MainCoordinator?
    var childCoordinators: [Coordinator] = [Coordinator]()
    var navigationController: UINavigationController
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = RestaurantsViewController.instantiate()
        vc.coordinator = self
        vc.tabBarItem = UITabBarItem(tabBarSystemItem: .topRated, tag: 0)
        navigationController.pushViewController(vc, animated: false)
      }
    
//    func didFinishBuying() {
//        parentCoordinator?.childDidFinish(self)
//    }
}

